FROM harbor.uio.no:443/library/docker.io-alpine

MAINTAINER Integrasjonsteamet <fs-utviklere@fsat.no>

RUN mkdir -p /conf

RUN apk update && apk upgrade && \
apk add tzdata && cp /usr/share/zoneinfo/Europe/Oslo /etc/localtime && \
apk add openjdk8 bash;

ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk
ENV LANG en_US.utf-8

ADD ./conf/app.properties /conf
ADD ./conf/secret.key /conf

ADD ./fsapi-jar-with-dependencies.jar /

EXPOSE 4567

# CMD java -jar -Dfs.conf=/conf /fsapi-jar-with-dependencies.jar

CMD java -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=2 -jar \
         -Dfs.conf=/conf \
         -DLOG_FOLDER=/log \
         /fsapi-jar-with-dependencies.jar

